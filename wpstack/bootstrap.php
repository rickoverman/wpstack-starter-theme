<?php

// include timber, cmb2 etc..
require_once(__DIR__ . '/vendor/autoload.php');


use Wpstack\App;
use Wpstack\Models;




// connect corcel
$params = array(
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'prefix'    => 'wp_' // default prefix is 'wp_', you can change to your own prefix
);
Corcel\Database::connect($params);




# run the Application
App::run();

// register the video custom post type and its particular class
// Corcel\Post::registerPostType('post', '\Wpstack\Models\Post');
// Corcel\Post::registerPostType('contact', '\Wpstack\Models\Contact');
// Corcel\Post::registerPostType('product', '\Wpstack\Models\Product');

// $contact = new Models\Contact;
// $contact->post_title = "Rick 666";
// $contact->save();
// $contact->meta->username = 'rickovermangfgssf';
// $contact->meta->url = 'http://overman.io';
// $contact->save();


// set the twig templates location to jade build destination
Timber::$dirname = basename(__DIR__).'/'.App::$config->jade->destination;

// add the timber context
add_filter('timber_context', 'add_to_context');
function add_to_context($data){

  // eloquent orm models and custom-post-types example
  $corcel['posts'] = Models\Post::all();
  $corcel['products'] = Models\Product::all();
  $corcel['contacts'] = Models\Contact::all();

  // Timber
  $data['corcel'] = $corcel;
  $data['menu'] = new TimberMenu();
  $data['categories'] = Timber::get_terms('category', 'show_count=0&title_li=&hide_empty=0&exclude=1');
  return $data;
}
