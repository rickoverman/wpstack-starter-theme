<?php

namespace Wpstack;

use \Symfony\Component\Yaml\Yaml;

class App {

  static $config;
  static $wpstack;

  public static function run()
  {

    self::$config = Yaml::parse(file_get_contents(__DIR__.'/../../config/build.yml'), false, false, true);
    get_class(self::$config); // stdClass


    self::setup();


    return self::$config;

  }

  public static function setup($file='wpstack.yml')
  {

    self::$wpstack = Yaml::parse(file_get_contents(__DIR__.'/../../config/'.$file), false, false, true);
    get_class(self::$wpstack); // stdClass



    return self::$wpstack;

  }


}
