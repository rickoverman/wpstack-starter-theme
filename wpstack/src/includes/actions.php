<?php

/**
 * add_action() or remove_action() calls.
 * ----------------------------------------------------------------------------
 */

remove_action( 'wp_head', 'wp_generator' );

add_action('pre_get_posts', 'vo_pre_get_posts');
add_action('after_setup_theme', 'remove_admin_bar');

// Override the gravity forms activation page
add_action('wp', 'vo_custom_maybe_activate_user', 9);

add_filter('gform_post_data', 'vo_set_post_type', 999, 2);
add_action( 'gform_pre_submission', 'post_pre_submission' );

function vo_pre_get_posts($query) {
    if ($query->is_search()) {
        if ($query->is_post_type_archive('event')) {
            $query->set('posts_per_page', 6);
        } else {
            $query->set('posts_per_page', 8);
        }
    }

    if (is_post_type_archive('user')) {
        $query->set('posts_per_page', 10);
    }

    if ($query->is_tax('themes')) {
        $query->set('posts_per_page', 8);
    }
}

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function vo_custom_maybe_activate_user() {
    $template_path = THEME_DIR . '/activate-user.php';
    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';

    if( ! file_exists( $template_path ) || ! $is_activate_page  )
        return;

    require_once( $template_path );

    exit();
}

function vo_set_post_type($post_data, $form)
{
    if ($form['id'] == get_option('share_form_' . ICL_LANGUAGE_CODE)) {
        global $vo_posttype;

        $post_data['post_type'] = $vo_posttype;
    }

    return $post_data;
}

function post_pre_submission($form)
{
    if ($form['id'] == get_option('share_form_' . ICL_LANGUAGE_CODE)) {
        global $vo_posttype;

        $vo_posttype = rgpost('input_3');
    }
}
