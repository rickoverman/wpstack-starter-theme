<?php

use VOCore\PostType\PostType;

$VO_core_docs = array(
    'id'        => 'resources',
    'args'      => array(
        'labels' => array(
            'name' => __( 'Resources' ),
            'singular_name' => __( 'Resource' ),
        ),
        'public' => true,
        'has_archive' => true,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions', 'tags' ),
        'menu_icon' => 'dashicons-admin-page',
        'rewrite' => array(
            'slug' => __( 'resources' )
        )
    )
);

new PostType( $VO_core_docs );
