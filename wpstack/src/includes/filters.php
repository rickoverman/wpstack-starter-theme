<?php

/**
 * add_filter() calls.
 * ----------------------------------------------------------------------------
 */

remove_filter( 'authenticate', 'wp_authenticate_username_password' );

add_filter( 'wp_title', 'vo_wp_title', 10, 2 );
add_filter( 'wp_mail_from', 'vo_new_mail_from' );
add_filter( 'wp_mail_from_name', 'vo_new_mail_from_name' );
add_filter( 'wpmu_signup_user_notification_subject', 'vo_my_activation_subject', 10, 4 );
add_filter( 'wpmu_signup_user_notification_email', 'vo_my_custom_email_message', 10, 4 );
add_filter( 'authenticate', 'wpse_115539_authenticate_username_password', 20, 3 );
add_filter( 'gform_next_button', 'vo_gform_custom_button', 10, 2 );
add_filter( 'gform_previous_button', 'vo_gform_previous_button', 10, 2 );
add_filter( 'gform_submit_button', 'vo_gform_custom_button', 10, 2 );
add_filter( 'gform_countries', 'vo_country_codes', 10, 1 );
add_filter( 'comment_author', 'dinalog_comment_author' );


/**
 * Custom functions called by add_filter() function.
 * ----------------------------------------------------------------------------
 */

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 */
function vo_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', THEME_SLUG ), max( $paged, $page ) );
    }

    return $title;
}

function vo_new_mail_from( $old ) {
    return get_option( 'admin_email' );
}

function vo_new_mail_from_name( $old ) {
    return get_option( 'blogname' );
}

function vo_my_activation_subject( $text ) {
    return __( 'Your account needs activation.', THEME_SLUG );
}

function vo_my_custom_email_message( $message, $user, $user_email, $key ) {
    $message  = __( 'To activate your new account, please click the following link:', THEME_SLUG ) . "\n\n";
    $message .= sprintf( '%s', site_url( "?page=gf_activation&key=$key" ) ) . "\n\n";
    $message .= sprintf( __( 'After you activate you will be able to log in at %s.', THEME_SLUG ), esc_url( home_url( '/' ) ) );

    return sprintf( $message );
}

function wpse_115539_authenticate_username_password( $user, $username, $password ) {
    if ( is_a( $user, 'WP_User' ) )
        return $user;

    if ( empty( $username ) || empty( $password ) ) {
        if ( is_wp_error( $user ) )
            return $user;

        $error = new WP_Error();

        if ( empty( $username ) )
            $error->add( 'empty_username', '<strong>ERROR</strong>: ' . __( 'The email address field is empty.', THEME_SLUG ) );

        if ( empty( $password ) )
            $error->add( 'empty_password', '<strong>ERROR</strong>: ' . __( 'The password field is empty.', THEME_SLUG ) );

        return $error;
    }

    $user = get_user_by( 'login', $username );

    if ( !$user )
        return new WP_Error( 'invalid_username', '<strong>ERROR</strong>: ' . __( 'Invalid email address.', THEME_SLUG ) );

    $user = apply_filters( 'wp_authenticate_user', $user, $password );

    if ( is_wp_error( $user ) )
        return $user;

    if ( ! wp_check_password( $password, $user->user_pass, $user->ID ) )
        return new WP_Error( 'incorrect_password', '<strong>ERROR</strong>: ' . __( 'The password you entered is incorrect.', THEME_SLUG ) );

    return $user;
}

function vo_gform_custom_button( $button, $form ) {
    $explode = explode( 'class=\'', $button );
    $button = implode( 'class=\'btn btn-info pull-right ', $explode );

    return $button;
}

function vo_gform_previous_button( $button, $form ) {
    $explode = explode( 'class=\'', $button );
    $button = implode( 'class=\'btn btn-info ', $explode );

    return $button;
}

function vo_gform_submit_button( $button, $form ) {
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);
    $new_button = $dom->createElement( 'button' );
    $button_span = $dom->createElement( 'span', $input->getAttribute( 'value' ) );
    $new_button->appendChild( $button_span );
    $input->removeAttribute( 'value' );
    foreach( $input->attributes as $attribute ) {
        $new_button->setAttribute( $attribute->name, $attribute->value );
    }
    $input->parentNode->replaceChild( $new_button, $input );
    return $dom->saveHtml( $new_button );
}

function vo_country_codes( $countries ) {
    $new_countries = array();

    foreach ( $countries as $country ) {
        $code                   = GF_Fields::get( 'address' )->get_country_code( $country );
        $new_countries[ $code ] = $country;
    }

    return $new_countries;
}


// Custom comment form fields
add_filter('comment_form_default_fields', 'custom_fields');
function custom_fields($fields) {

    $req        = get_option( 'require_name_email' );
    $aria_req   = ( $req ? " aria-required='true'" : '' );

    $fields['author'] = '<p class="comment-form-author">'.
                        '<label for="author">' . __( 'First name', THEME_SLUG ) . '</label>'.
                        ( $req ? '<span class="required">*</span>' : '' ).
                        '<input id="author" name="author" type="text" size="30" tabindex="1"' . $aria_req . ' /></p>';

    $fields['url'] =    '<p class="comment-form-url">'.
                        '<label for="url">' . __( 'Last name', THEME_SLUG ) . '</label>'.
                        ( $req ? '<span class="required">*</span>' : '' ).
                        '<input id="url" name="url" type="text" size="30" tabindex="2"' . $aria_req . ' /></p>';

    $fields['email'] =    '<input id="email" name="email" type="text" size="30" value="comment@amsterdamlogistics.nl" tabindex="2"' . $aria_req . ' hidden /></p>';

  return $fields;
}

function dinalog_comment_author($username)
{
    $user = get_user_by('login', $username);


    if (empty($user)) {
        return $username;
    }

    $salesforce_id = get_user_meta($user->ID, '_salesforce_Id', true);

    if (empty($salesforce_id)) {
        return $username;
    }

    $salesforce_user = \connectpush_dinalog\SalesforceApi::getContactFromSalesforce($salesforce_id);

    return $salesforce_user->FirstName . ' ' . $salesforce_user->LastName;
}
