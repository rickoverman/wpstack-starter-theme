<?php

function button( $atts, $content = '' ) {
    $atts = shortcode_atts( array(
        'link'      => '#',
        'color'     => 'blue',
    ), $atts, 'button' );

    switch( $atts['color'] ) {
        case 'blue':
            $class = 'btn-info';
            break;
        case 'orange':
            $class = 'btn-warning';
            break;
        default:
            $class = 'btn-info';
            break;
    }

    $output .= '<a class="btn ' . $class . '" href="' . esc_url( $atts['link'] ) . '">' . $content . '</a>';

    return $output;
}
add_shortcode( 'button', 'button' );
