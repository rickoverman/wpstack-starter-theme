/**
 * App.js Main javascript file
 * @author Rick Overman (rick@van-ons.nl)
 * @copyright 2016 Van Ons BV.
 */

// load a log utility
log = require('./modules/log.js')


// Load the app configuration
// var config = require('./../config/app.json');



var MobileDetect = require('mobile-detect'),
    md = new MobileDetect(window.navigator.userAgent);



var foundation = require('foundation-sites');
// var $ = jQuery = require('jquery');
// ajaxify = require('ajaxify');



// display console log header
log.head();


// log debug status
// console.log('debug', config.debug);


// initialize the foundation frameworl
$(document).foundation();


// log file load
console.log('Loaded app.js');
